/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

/*
     ---- ``````````````````````````````````````````````````````````````````````````````````````
     ----`                                                                     `-    ````````` `
         `                                                                     :+    .----/:--``
     +///`                                                                     /-.   .---..-..`.
         `                                                                    .:`/   `   `/--.``
     `````                                                                    :` /   .....--:-``
     --/:`                                                                    /  --            .
         `                              CCA                                  --   /            `
     :---`                              ANALYSIS                             /  ` /`           .
  .. ....`                                                                  `: .. .-           `
  ++     `                                                                  :.`-/. /           `
  -/ ////`                                                                  / -+::`:`          .
 `+/     `                                                                 .:.+` +-`:          `
  //``````                                                                 :.+.  .+`/          `
  /+`::/:`                                                                 /::    :::.         .
 `s/     `                                                                -//      +.:         `
     ----`                                                                //       .//         .
     ....`                                                               `/`        :/.        `
         `                                                               ..          :/        `
     ////`                                                              `:           `/        .
         `                                                             .::            ..       `
     `````                     .::-.`                                -:.:`             -       `
     --::`                  `:+/-.--:/:-.` ```...........`        `::..-:              -/::-----
         .::--::------.```-:-.         `.:::::::-------:://::::-.:/--.                 `----:---
       `-`-.`..-:::---...-``````````````````````--..........`..--``````````````````````````````.
        .-.     :`:      :     `:`-     --.    -: :    `.-    .....    -`.    -..-`    --`    -: :
         .`     .`.      .      .`. `:..-:.`...-/.-:-...--..``-`-//..  `.`    `.`.`    `.`    `.`.
                                    `..--::-:-.-.:`-:--/------: -.-:-
*/

#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "arm_math.h"
#include "defines.h"
#include "init.h"
#include "handlers.h"
#include "stm_related_functions.h"
#include "ads1298_related_functions.h"
#include "aux_functions.h"
#include "defines_cca.h"
#include "ini_cca.h"
#include "mat_ops.h"
#include "cca.h"

#include <stdio.h>
#include <stm32f4xx.h>
#include "cca.h"
#include "dma.h"


/** DATA BUFFERS ***********************************************/
float X[N_CHANNELS][EV_WINDOWS_SIZE] = {{0}};
float reception_matrix_A[N_CHANNELS][SHIFT_WINDOW_SIZE];
float reception_matrix_B[N_CHANNELS][SHIFT_WINDOW_SIZE];

float32_t ADDdataM[N_CHANNELS][SHIFT_WINDOW_SIZE];
ring_buffer_t ADDdataM2[N_CHANNELS];
/** ************************************************************/


/** FUNCTION DECLARATIONS **************************************/
static void SysTickConfig(void);
void Configure_B3(void);
void classify_bci_output(void);
/** ************************************************************/


/** FLAGS AND COUNTERS *****************************************/
int new_window_available = 0;
int new_window = 0;
int micro_window_counter = 0;
int temp_downsample = 1;
int threshold = 0.55;
int32_t trigger = 0;
float SVM_output = 0;
int tics_sampling = 0;
/** ************************************************************/


/** DMA ********************************************************/
DMA_Stream_TypeDef* streams[] = {DMA2_Stream0,DMA2_Stream1};
int dma_working = 0;
/** ************************************************************/




/** FILTERS ****************************************************/
float32_t firCoeffs[NTAPS] = { -0.00098270593855419243f, -0.00039976295817230800f, -0.00043000208548029338f, -0.00041899690106067804f, -0.00035611784839050908f, -0.00023310948157800480f, -0.00004505884823455123f, 0.00020859950488717478f, 0.00052355791611649063f, 0.00088960681460607447f, 0.00128982366444500250f, 0.00170068658011439370f, 0.00209347617698784140f, 0.00243573360537980380f, 0.00269072433913069080f, 0.00281996110496269110f, 0.00279068657341356500f, 0.00256939257530537170f, 0.00213359038025884560f, 0.00146889862233976970f, 0.00057427021023392867f, -0.00053673056414665064f, -0.00183451579614569940f, -0.00327244906926165650f, -0.00478717451643301870f, -0.00629980273702382670f, -0.00771858863629262640f, -0.00894183257727982140f, -0.00986165119138286690f, -0.01036988160910970500f, -0.01036298949267520900f, -0.00974682134401893590f, -0.00844428696436174890f, -0.00639833560342171260f, -0.00357840846442749730f, 0.00001701455899918644f, 0.00435759166229383700f, 0.00938003964649871370f, 0.01498890139799274400f, 0.02105842661484635900f, 0.02743675346770321100f, 0.03395045011311425400f, 0.04041126589093037300f, 0.04662368700175663300f, 0.05239222458898600000f, 0.05753009244161329200f, 0.06186729387761264600f, 0.06525730034268979700f, 0.06758454623930504300f, 0.06876866439937920400f, 0.06876866439937920400f, 0.06758454623930504300f, 0.06525730034268979700f, 0.06186729387761264600f, 0.05753009244161329200f, 0.05239222458898600000f, 0.04662368700175663300f, 0.04041126589093037300f, 0.03395045011311425400f, 0.02743675346770321100f, 0.02105842661484635900f, 0.01498890139799274400f, 0.00938003964649871370f, 0.00435759166229383700f, 0.00001701455899918644f, -0.00357840846442749730f, -0.00639833560342171260f, -0.00844428696436174890f, -0.00974682134401893590f, -0.01036298949267520900f, -0.01036988160910970500f, -0.00986165119138286690f, -0.00894183257727982140f, -0.00771858863629262640f, -0.00629980273702382670f, -0.00478717451643301870f, -0.00327244906926165650f, -0.00183451579614569940f, -0.00053673056414665064f, 0.00057427021023392867f, 0.00146889862233976970f, 0.00213359038025884560f, 0.00256939257530537170f, 0.00279068657341356500f, 0.00281996110496269110f, 0.00269072433913069080f, 0.00243573360537980380f, 0.00209347617698784140f, 0.00170068658011439370f, 0.00128982366444500250f, 0.00088960681460607447f, 0.00052355791611649063f, 0.00020859950488717478f, -0.00004505884823455123f, -0.00023310948157800480f, -0.00035611784839050908f, -0.00041899690106067804f, -0.00043000208548029338f, -0.00039976295817230800f, -0.00098270593855419243f};
float32_t iirCoeffs[5] = {0.98238543852609173f, -1.964770877052184f, 0.98238543852609173f, 1.9644605802052322f,   -0.96508117389913495f};

arm_biquad_cascade_df2T_instance_f32 IIR[N_CHANNELS];
arm_fir_decimate_instance_f32 FIR[N_CHANNELS];
/** ************************************************************/

int main(void)
{

#ifdef PROFILE_SYSTEM

    // init, reset and start the cycle counter
    *SCB_DEMCR = *SCB_DEMCR | 0x01000000;
    *DWT_CYCCNT = 0; 							// reset the counter
    *DWT_CONTROL = *DWT_CONTROL | 1 ; 			// enable the counter

    //STOPWATCH_START
	//STOPWATCH_STOP

#endif

    for(int i = 0; i < N_CHANNELS; i++) ring_buffer_init(&ADDdataM2[i]);	//init ring buffers
   SysTickConfig();
   GPIO_Config_LEDS();					//config the leds
   GPIO_Config_REGULATOR_DRIVERS();		//config IMU and BT LDO-act pins
   GPIO_SetBits(GPIOD, GPIO_Pin_13);   	//Enable BT LDO
   USART_Config();					   	//Config Usart
   USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);	//Enable INT USART
   ADS_SPI_Config_DMA();					//Config SPI for ADS comm
   SPI_NVIC_Config();					//nVic
   ADS_Config_CONTROL_PINS();			//config_Ads_pin_controls
   GPIO_ToggleBits( GPIOA, GPIO_Pin_8); //LED BLINK
   GPIO_SetBits(GPIOE, GPIO_Pin_10);   	//TURN ON ADS
   GPIO_SetBits(GPIOE, GPIO_Pin_9);   	//PUT ADS OUT OF RESET STATE
   MAIN_FSM_state = MAIN_FSM_INIT;		//ini State Machine
   mando = 10;
   CommTxBufferIndex = 0;
   SPI_Rx_maxV = SPI_RX_BUFFER_SIZE;
   SPI_Tx_maxV = SPI_TX_BUFFER_SIZE;
   delay_500ms();
   GPIO_ResetBits(GPIOE, GPIO_Pin_9);   //PUT ADS IN RESET STATE
   delay_500ms();
   GPIO_SetBits(GPIOE, GPIO_Pin_9);   	//PUT ADS OUT OF RESET STATE
   en_timeou = 0 ;
   Configure_B3();
   STM_EVAL_LEDInit(LED5);

   //Initialize filters
   // Low pass FIR
   	float32_t firStates[N_CHANNELS][NTAPS+BLOCK_SIZE_FIR-1];
   	for (int chInx = 0; chInx<N_CHANNELS; chInx++) {
   		arm_fir_decimate_init_f32(&FIR[chInx], NTAPS, DS, firCoeffs, firStates[chInx], BLOCK_SIZE_FIR);
   	}

   	// High pass IIR
   	float32_t iirStates[N_CHANNELS][2*1] = {{0}};
   	for (int chInx = 0; chInx<N_CHANNELS; chInx++) {
   		arm_biquad_cascade_df2T_init_f32(&IIR[chInx], 1, iirCoeffs, iirStates[chInx]);
   	}

   	/* BEGIN DMA */
	uint32_t channels[] = {DMA_Channel_0,DMA_Channel_1};
	uint32_t incPers[] = {1,1};
	uint32_t incMems[] = {1,1};
	DMA_Config(streams, channels, incPers, incMems);
	/* END DMA */

   while(1)
   {

      switch(MAIN_FSM_state)
      {



            case MAIN_FSM_INIT:

             current_RX_buffer_length = 1;

             if(en_timeou >= 500)
                  {
                     GPIO_ResetBits(GPIOB,GPIO_Pin_11);   	//ENABLE ADS CS
                     MAIN_FSM_state = MAIN_FSM_ADS_CONF;	//pass to the next state machine
                     en_timeou = 0 ;
                  }
             break;



            case MAIN_FSM_ADS_CONF:
            	config_ads();				//Config all the registers
            	break;


            case MAIN_FSM_IDLE:

            	//stream_enable = 1;

            	if(stream_enable)
            	{

                  MAIN_FSM_state = MAIN_FSM_STREAM;		//program next move to transmit the received data from ADS
                  SPI_Rx_maxV = SPI_DATA_SIZE;        	//SETUP BUFFER LENGTH  27 BYTEs (3 STATUS) + (8 CHANNELS * 3 DATABYTES)
                  SPI_Tx_maxV = SPI_DATA_SIZE;

                  SPI_RX_GOING = 1;                		//Configure indexes and flags
                  SPI_TxIndex = 0;
                  SPI_RxIndex = 0;

                  GPIO_SetBits(GPIOE, GPIO_Pin_8);      //SET START HIGH
                  SPI_DRDY_EXTI_Config();           	//ENABLE INTERRUPT ON DRDY HIGH

                  ADS_SEND_RDATAC();                	//SEND RDATAC
                  SPI_I2S_ReceiveData(SPI2);         	//RESET INTERRUPT FOR READING

                  SPI_TxBuffer[0] = 0;
                  SPI_TxBuffer[1] = 0;
                  SPI_TxBuffer[2] = 0;

               }

               break;




            case MAIN_FSM_STREAM:



            	//ADC IS CONFIGURED, PREPARE TO RECEIVE DATA IN CONTINUOUS MODE//

            	if(new_window)
            	{
            		new_window=0;

            		dowhatyouhavetodoCCA();


            	}




               if(!stream_enable)									//reset everything when no data is requested by the user
               {
            	   MAIN_FSM_state = MAIN_FSM_IDLE;					//go to idle in the state machine
                   GPIO_ResetBits(GPIOE, GPIO_Pin_8);      			//SET START HIGH
                   ADS_SEND_SDATAC();                					//SEND SDATAC
                   SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_RXNE, DISABLE);	//disable RX interrupt from SPI
                   SPI_I2S_ITConfig(SPI2, SPI_I2S_IT_TXE, DISABLE);	//disable TX interrupt from SPI

                   ADS_SEND_SDATAC();                					//SEND SDATAC
                   SPI_I2S_ReceiveData(SPI2);         				//RESET INTERRUPT FOR READING
               }

/*
               if(dma_working)
            	   __WFI();
               else
            	   PWR_EnterSTOPMode(PWR_Regulator_ON,PWR_STOPEntry_WFI); //GO TO SLEEP
*/
               break;




            	case MAIN_FSM_WAIT:											//not used

            	break;


      }

   }

}





static void SysTickConfig(void)
{
	/* Setup SysTick Timer for 10ms interrupts  */
	if (SysTick_Config(SystemCoreClock / 1000))
	{
		/* Capture error */
		while (1);
	}
	/* Configure the SysTick handler priority */
	//NVIC_SetPriority(SysTick_IRQn, 0x0);
}





void Configure_B3(void) {
    /* Set variables used */
    GPIO_InitTypeDef GPIO_InitStruct;
    EXTI_InitTypeDef EXTI_InitStruct;
    NVIC_InitTypeDef NVIC_InitStruct;

    /* Enable clock for GPIOB */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    /* Enable clock for SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Set pin as input */
    GPIO_InitStruct.GPIO_Mode = 	GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_OType = 	GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Pin = 		GPIO_Pin_3;
    GPIO_InitStruct.GPIO_PuPd = 	GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_Speed = 	GPIO_Speed_100MHz;
    GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Tell system that you will use PB12 for EXTI_Line12 */
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource3);

    /* PB12 is connected to EXTI_Line12 */
    EXTI_InitStruct.EXTI_Line = EXTI_Line3;
    /* Enable interrupt */
    EXTI_InitStruct.EXTI_LineCmd = ENABLE;
    /* Interrupt mode */
    EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
    /* Triggers on rising and falling edge */
    EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    /* Add to EXTI */
    EXTI_Init(&EXTI_InitStruct);

    /* Add IRQ vector to NVIC */
    /* PB12 is connected to EXTI_Line12, which has EXTI15_10_IRQn vector */
    NVIC_InitStruct.NVIC_IRQChannel = EXTI3_IRQn;
    /* Set priority */
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
    /* Set sub priority */
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x01;
    /* Enable interrupt */
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    /* Add to NVIC */
    NVIC_Init(&NVIC_InitStruct);
}




/* Handle PB12 interrupt */
void EXTI3_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line3) != RESET) {
        /* Do your stuff when PB12 is changed */

    	GPIO_ToggleBits( GPIOA, GPIO_Pin_8);

    	if(trigger == 0) trigger = 200;
    	else
    		trigger = 0;


        /* Clear interrupt flag */
        EXTI_ClearITPendingBit(EXTI_Line3);
    }
}




void dowhatyouhavetodoCCA(){

	float32_t X_filtered[N_CHANNELS][SHIFT_WINDOW_SIZE/DS] = {{0}};

	if (ENABLE_FILTERS) {
		filterData(ADDdataM, X_filtered, FIR, IIR);
	}
	else {
		downsampleData(ADDdataM, X_filtered);
	}

	copy_samples(X, X_filtered, ADDdataM2);
	process_cca(X, correlations);

	classify_bci_output();


}

void classify_bci_output() {

	//output the values approx at the same time once available.
	float average_value = 0;

	average_value = average_value/(int)N_FREQs;

	int max_index = max_inx (correlations, (int) N_FREQs);
	float max_value = correlations[max_index];

	if(max_value > (threshold*average_value))
	{
		switch(max_index)
		{
			// FERQ 1
			case 0:
				GPIO_SetBits( GPIOA, GPIO_Pin_8); //LED D1 ON
				break;

			//FREQ 2
			case 2:
				GPIO_SetBits( GPIOA, GPIO_Pin_9); //LED D2 ON
				break;

			//FREQ 3
			case 6:
				GPIO_SetBits( GPIOA, GPIO_Pin_8 | GPIO_Pin_9); //ALL LED ON
				break;

			// FREQ 4
				// LEDs missing...

			default:
				GPIO_ResetBits( GPIOA, GPIO_Pin_8 | GPIO_Pin_9); //ALL LED OFF
				break;
		}


	}
	else GPIO_ResetBits( GPIOA, GPIO_Pin_8 | GPIO_Pin_9); //ALL LED OFF
}




