/*	Modified from https://github.com/AndersKaloer/Ring-Buffer	*/

#include "ringbuffer.h"

/**
 * @file
 * Implementation of ring buffer functions.
 */

void ring_buffer_init(ring_buffer_t *buffer) {
  buffer->tail_index = 0;
  buffer->head_index = 0;
  buffer->full_flag = 0;

  for(int i = 0; i < RING_BUFFER_SIZE; i++) buffer->buffer[i] = 0;
}

void ring_buffer_queue(ring_buffer_t *buffer, RING_BUFFER_TYPE data) {
  /* Is buffer full? */
  if(ring_buffer_is_full(buffer))
  {
	/* Is going to overwrite the oldest byte */
	/* Increase tail index */
	buffer->tail_index = ((buffer->tail_index + 1) & RING_BUFFER_MASK);
  }
  else if(((buffer->head_index - buffer->tail_index) & RING_BUFFER_MASK) == RING_BUFFER_MASK) {
	buffer->full_flag = 1;
  }

  /* Place data in buffer */
  buffer->buffer[buffer->head_index] = data;
  buffer->head_index = ((buffer->head_index + 1) & RING_BUFFER_MASK);
}

void ring_buffer_queue_arr(ring_buffer_t *buffer, const RING_BUFFER_TYPE *data, ring_buffer_size_t size) {
  /* Add bytes; one by one */
  ring_buffer_size_t i;
  for(i = 0; i < size; i++) {
    ring_buffer_queue(buffer, data[i]);
  }
}

uint8_t ring_buffer_dequeue(ring_buffer_t *buffer, RING_BUFFER_TYPE *data) {
  if(ring_buffer_is_empty(buffer)) {
    /* No items */
    return 0;
  }

  if(ring_buffer_is_full(buffer)) {
	*data = buffer->buffer[buffer->head_index];
	buffer->full_flag = 0;
    return 1;
  }

  *data = buffer->buffer[buffer->tail_index];
  buffer->tail_index = ((buffer->tail_index + 1) & RING_BUFFER_MASK);
  return 1;
}

ring_buffer_size_t ring_buffer_dequeue_arr(ring_buffer_t *buffer, RING_BUFFER_TYPE *data, ring_buffer_size_t len) {
  if(ring_buffer_is_empty(buffer)) {
    /* No items */
    return 0;
  }

  RING_BUFFER_TYPE *data_ptr = data;
  ring_buffer_size_t cnt = 0;
  while((cnt < len) && ring_buffer_dequeue(buffer, data_ptr)) {
    cnt++;
    data_ptr++;
  }
  return cnt;
}

uint8_t ring_buffer_peek(ring_buffer_t *buffer, RING_BUFFER_TYPE *data, ring_buffer_size_t index) {
  ring_buffer_size_t items_num;

  if(ring_buffer_num_items(buffer,&items_num) == 1) {
	  if(index == RING_BUFFER_SIZE - 1) {
	  	  *data = buffer->buffer[buffer->head_index];
	  	  return 1;
	  }
	  items_num = RING_BUFFER_SIZE - 1;
  }

  if(index >= items_num) {
    /* No items at index */
    return 0;
  }

  /* Add index to pointer */
  ring_buffer_size_t data_index = ((buffer->tail_index + index) & RING_BUFFER_MASK);
  *data = buffer->buffer[data_index];
  return 1;
}

ring_buffer_size_t ring_buffer_peek_arr(ring_buffer_t *buffer, RING_BUFFER_TYPE *data, ring_buffer_size_t index, ring_buffer_size_t len) {
  if(ring_buffer_is_empty(buffer)) {
    /* No items */
    return 0;
  }

  RING_BUFFER_TYPE *data_ptr = data;
  ring_buffer_size_t cnt = 0;
  while((cnt < len) && ring_buffer_peek(buffer, data_ptr, index + cnt)) {
    cnt++;
    data_ptr++;
  }
  return cnt;
}

uint8_t ring_buffer_num_items(ring_buffer_t *buffer, ring_buffer_size_t *result) {
  *result = (buffer->head_index - buffer->tail_index) & RING_BUFFER_MASK;
  return (buffer->full_flag);
}

extern inline uint8_t ring_buffer_is_empty(ring_buffer_t *buffer);
extern inline uint8_t ring_buffer_is_full(ring_buffer_t *buffer);
