/*
 * mat_ops.c
 *
 *  Created on: 23 Apr 2018
 *      Author: matti
 */

#include "mat_ops.h"
#include "arm_math.h"
#include "profileSTM.h"

#include <math.h>

#pragma GCC push_options
#pragma GCC optimize ("O2")

void mat_mult_t1_tres(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_cols_m2]) {

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};

	float mat_2t[n_rows_m2][n_cols_m2];
	arm_matrix_instance_f32 srcB_t = {n_rows_m2, n_cols_m2, (float32_t *)mat_2t};
	arm_mat_trans_f32(&srcB, &srcB_t);

	arm_matrix_instance_f32 dstC = {n_cols_m1, n_cols_m2, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcA, &srcB_t, &dstC);

}

void mat_mult_t1(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_cols_m1]) {

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	float mat_1t[n_rows_m1][n_cols_m1];
	arm_matrix_instance_f32 srcA_t = {n_rows_m1, n_cols_m1, (float32_t *)mat_1t};
	arm_mat_trans_f32(&srcA, &srcA_t);

	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};
	arm_matrix_instance_f32 dstC = {n_cols_m2, n_cols_m1, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcB, &srcA_t, &dstC);

}

void mat_mult(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_rows_m1]) {

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};
	arm_matrix_instance_f32 dstC = {n_cols_m2, n_rows_m1, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcB, &srcA, &dstC);

}

void mat_sub(int n_rows, int n_cols, float a[][n_rows], float b[][n_rows], float r[][n_rows]) {

	arm_matrix_instance_f32 srcA = {n_cols, n_rows, (float32_t *)a};
	arm_matrix_instance_f32 srcB = {n_cols, n_rows, (float32_t *)b};
	arm_matrix_instance_f32 srcR = {n_cols, n_rows, (float32_t *)r};
	arm_mat_sub_f32(&srcA, &srcB, &srcR);

}

float norm(float* x, int begin_index, int end_index) {

	float res;
	int block_size = end_index-begin_index;
	arm_power_f32(&(x[begin_index]), block_size, &res);
	arm_sqrt_f32(res, &res);

	return res;

}

void center_variable(float* X, int length) {

	float vec_mean;
	arm_mean_f32(X,length,&vec_mean);
	arm_offset_f32(X,-vec_mean,X,length);

}

#pragma GCC pop_options
