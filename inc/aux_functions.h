#ifndef AUX_FUNCTIONS_H_
#define AUX_FUNCTIONS_H_

#include "stm32f4xx.h"

extern int ms500_flag;


void write_bt_command(char* commandString, int length_command);
void delay_500ms();
int max_inx(float array[], int size);



#endif /* AUX_FUNCTIONS_H_ */
