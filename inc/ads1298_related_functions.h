/*
 * ads1298_related_functions.h
 *
 *  Created on: Nov 21, 2017
 *      Author: Micrel
 */

#ifndef ADS1298_RELATED_FUNCTIONS_H_
#define ADS1298_RELATED_FUNCTIONS_H_

#include "stm32f4xx.h"
#include "defines.h"

extern char config_reg;
extern char config_value1;
extern char config_value2;
extern char config_value3;
extern int en_timeou;
extern char lead_off;
extern MAIN_FSM MAIN_FSM_state;
extern int ms100_flag;
extern int SPI_RX_GOING;
extern char SPI_RxBuffer[SPI_BUFFER_MAXSIZE];
extern uint8_t SPI_RxIndex;
extern uint8_t SPI_TxIndex;
extern char SPI_Rx_maxV;
extern char SPI_Tx_maxV;
extern int SPI_TX_GOING;
extern char SPI_TxBuffer[SPI_BUFFER_MAXSIZE];
extern int tic_config_100ms;


void config_ads(void);
void ADS_SEND_RDATA(void);
void ADS_SEND_RDATAC(void);
void ADS_SEND_SDATAC(void);
char ADS_REGISTER_READ(char reg_in);
char ADS_REGISTER_WRITE(char reg_addr, char reg_value);
void ADS_REGISTER_READ_INT(char reg_addr);
void ADS_REGISTER_WRITE_INT(char reg_addr, char reg_value);
void ADS_SPI_Config(void);
void ADS_SPI_Config_DMA(void);
void ADS_Config_CONTROL_PINS(void);
void ADS_Read_DMA();

#endif /* ADS1298_RELATED_FUNCTIONS_H_ */
