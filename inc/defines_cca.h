/*
 * defines_cca.h
 *
 *  Created on: Dec 7, 2017
 *      Author: Micrel
 */

#ifndef DEFINES_CCA_H_
#define DEFINES_CCA_H_

#include <math.h>

/** APPLICATION PARAMETERS *****************************/
#define ACQ_WINDOW_SIZE		2000							// Acquisition window length for each CCA
#define SHIFT_WINDOW_SIZE   200 							// Length of the acquisition window shift between two consecutive processing
#define DS					10								// Downsample factor of the acquisition window
#define EV_WINDOWS_SIZE 	(ACQ_WINDOW_SIZE/DS) 			// Actual size of the evaluation window in input to the CCA after downsampling
#define N_CHANNELS 			3								// Number of acquisition channels
#define ENABLE_FILTERS 		1									// Debug flag (MUST BE ALWAYS 1)
/** ***************************************************/


/** SYSTEM PARAMETERS *********************************/
#define FS 					1000							// Sample frequency
#define MAX_N_CHANNELS		8								// Maximum number of channels
/** ***************************************************/


/** PROCESSING PARAMETERS *****************************/
#define N_FREQs				4								// Number of stimuli used in the BCI
#define N_HARMONICS 		2								// Number of harmonics taken into account in reference signal (including fundamental frequency)
#define N_REF_SIGNALS 		N_HARMONICS*2					// Number of reference signals for each frequency (sine and cosine of each frequency harmonics)
#define MAX_N_FREQs			8								// Maximum number of stimuli for the BCI
#define NTAPS 				100								// Number of taps for the FIR filter
#define BLOCK_SIZE_FIR 		40
#if BLOCK_SIZE_FIR > (SHIFT_WINDOW_SIZE/DS)
#define BLOCK_SIZE_IIR 		(SHIFT_WINDOW_SIZE/DS)
#else
define BLOCK_SIZE_IIR BLOCK_SIZE_FIR
#endif
#define FIR_NUM_BLOCKS 		(SHIFT_WINDOW_SIZE/BLOCK_SIZE_FIR)
#define IIR_NUM_BLOCKS 		((SHIFT_WINDOW_SIZE/DS)/BLOCK_SIZE_IIR)
/** ***************************************************/

#if (MICRO_WINDOW_SIZE % DS) != 0
#error "DOWNSAMPLE_FACTOR must be a multiple of SHIFT_WINDOW_SIZE, otherwise code could not work"
#endif



#if (MICRO_WINDOWS_SIZE % BLOCK_SIZE_FIR + BLOCK_SIZE_FIR % DS + BLOCK_SIZE_FIR % 8) != 0
#error "BLOCK_SIZE_FIR must be a multiple of MICRO_WINDOWS_SIZE. DS and 8 must be multiples of BLOCK_SIZE_FIR"
#endif

#if (BLOCK_SIZE_FIR % 8) != 0
#warning "8 should be multiples of BLOCK_SIZE_FIR"
#endif

#if ((MICRO_WINDOW_SIZE/DS) % BLOCK_SIZE_IIR) != 0
#error "BLOCK_SIZE_IIR must be a multiple of MICRO_WINDOWS_SIZE and 8 must be multiples of BLOCK_SIZE_IIR"
#endif

#if (BLOCK_SIZE_IIR % 8) != 0
#warning "BLOCK_SIZE_IIR should be multiple of 8"
#endif


#endif /* DEFINES_CCA_H_ */
