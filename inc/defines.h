/*
 * includes.h
 *
 *  Created on: Sep 4, 2016
 *      Author: Javier
 */

#ifndef DEFINES_H_
#define DEFINES_H_

//defines for the resgisters of the ADS1298
#define RDATA   0x12
#define RDATAC  0x10
#define SDATAC  0x11
#define WREG    0x40
#define RREG    0x20

#define ID      0x00
#define CONFIG1 0x01
#define CONFIG2 0x02
#define CONFIG3 0x03
#define LOFF    0x04
#define CH1SET  0x05
#define CH2SET  0x06
#define CH3SET  0x07
#define CH4SET  0x08
#define CH5SET  0x09
#define CH6SET  0x0A
#define CH7SET  0x0B
#define CH8SET  0x0C
#define RLD_SENSP 0x0D
#define RLD_SENSN 0x0E
#define LOFF_SENSP 0x0F
#define LOFF_SENSN 0x10
#define LOFF_FLIP 0x11
#define LOFF_STATP 0x12
#define LOFF_STATN 0x13
#define GPIO    0x14
#define PACE    0x15
#define RESP    0x16
#define CONFIG4 0x17
#define WCT1    0x18
#define WCT2    0x19

#define CONFIG1_HR          0x80
#define CONFIG1_DAISYEN     0x40
#define CONFIG1_CLKEN       0x20
#define CONFIG1_DR2         0x04
#define CONFIG1_DR1         0x02
#define CONFIG1_DR0         0x01

#define CONFIG2_WCTCHOP     0x20
#define CONFIG2_INTTEST     0x10
#define CONFIG2_TESTAMP     0x04
#define CONFIG2_TESTFREQ1   0x02
#define CONFIG2_TESTFREQ0   0x01
#define CONFIG2_DEFAULT     0x00

#define CONFIG3_PD_REFBUF   0x80
#define CONFIG3_VREF_4V     0x20
#define CONFIG3_RLD_MEAS    0x10
#define CONFIG3_RLDREF_INT  0x08
#define CONFIG3_PD_RLD      0x04
#define CONFIG3_RLD_LOFF_SENS 0x02
#define CONFIG3_RLD_STAT    0x01
#define CONFIG3_DEFAULT     0x40

#define LOFF_COMP_TH2       0x80
#define LOFF_COMP_TH1       0x40
#define LOFF_COMP_TH0       0x20
#define LOFF_VLEAD_OFF_EN   0x10
#define LOFF_ILEAD_OFF1     0x08
#define LOFF_ILEAD_OFF0     0x04
#define LOFF_FLEAD_OFF1     0x02
#define LOFF_FLEAD_OFF0     0x01
#define LOFF_ENABLE

#define CHNSET_PD           0x80
#define CHNSET_GAIN2        0x40
#define CHNSET_GAIN1        0x20
#define CHNSET_GAIN0        0x10
#define CHNSET_MUXN2        0x04
#define CHNSET_MUXN1        0x02
#define CHNSET_MUXN0        0x01
#define CHNSET_DEFAULT      0x00

#define GPIO_GPIOD4         0x80
#define GPIO_GPIOD3         0x40
#define GPIO_GPIOD2         0x20
#define GPIO_GPIOD1         0x10
#define GPIO_GPIOC4         0x08
#define GPIO_GPIOC3         0x04
#define GPIO_GPIOC2         0x02
#define GPIO_GPIOC1         0x01

#define PACE_PACEE1         0x10
#define PACE_PACEE0         0x08
#define PACE_PACEO1         0x04
#define PACE_PACEO0         0x02
#define PACE_PD_PACE        0x01

#define RESP_DEMOD_EN1      0x80
#define RESP_MOD_EN1        0x40
#define RESP_DEFAULT        0x20
#define RESP_PH2            0x10
#define RESP_PH1            0x08
#define RESP_PH0            0x04
#define RESP_CTRL1          0x02
#define RESP_CTRL0          0x01

#define CONFIG4_RESP_FREQ2  0x80
#define CONFIG4_RESP_FREQ1  0x40
#define CONFIG4_RESP_FREQ0  0x20
#define CONFIG4_SINGLE_SHOT 0x08
#define CONFIG4_WCT_TO_RLD  0x04
#define CONFIG4_PD_LOFF_COMP 0x02

#define WCT1_AVF_CH6        0x80
#define WCT1_AVL_CH5        0x40
#define WCT1_AVR_CH7        0x20
#define WCT1_AVR_CH4        0x10
#define WCT1_PD_WCTA        0x08
#define WCT1_WCTA2          0x04
#define WCT1_WCTA1          0x02
#define WCT1_WCTA0          0x01

#define WCT2_PD_WCTC        0x80
#define WCT2_PD_WCTB        0x40
#define WCT2_WCTB2          0x20
#define WCT2_WCTB1          0x10
#define WCT2_WCTB0          0x08
#define WCT2_WCTC2          0x04
#define WCT2_WCTC1          0x02
#define WCT2_WCTC0          0x01

// Bit positions of the lead off status bits
#define LOFFRA 15
#define LOFFLA 14
#define LOFFLL 13
#define LOFFRL 12
#define LOFFV1 11
#define LOFFV2 10
#define LOFFV3 9
#define LOFFV4 8
#define LOFFV5 7
#define LOFFV6 6

//defines for the UART control
#define START_STREAM_CMD		0x3D    // "=" in ASCII
#define STOP_STREAM_CMD			0x3A   	// ":" in ASCII
#define PACKET_REQUESTED		0x72   	// "r" in ASCII
#define NO_ACC_CMD				0x6E   	// "r" in ASCII
#define CONFIG_CMD   	  		0x23    // "#" in ASCII
#define COMM_RX_BUFFER_SIZE 	256
#define COMM_TX_BUFFER_SIZE 	256
#define SPI_BUFFER_MAXSIZE 35
#define START 	0x20;
#define PKT_ID 	0x0A;

//defines for the SPI
#define SPI_RX_BUFFER_SIZE 3
#define SPI_DATA_SIZE 27
#define SPI_BUFFER_MAXSIZE 35
#define SPI_TX_BUFFER_SIZE 3

//custom defines must be added here.
#define DOWNSAMPLE_FACTOR_STREAM 1



//for convenience, some datatypes here


//UART control typedef
typedef enum
{
   USART_IDLE_STATUS,
   USART_STREAM_STATUS,
   USART_CMD_STATUS,
   USART_INIT_INT
} USART_STATUS;

//State Machine control typedef
typedef enum
{
   MAIN_FSM_INIT,
   MAIN_FSM_ADS_CONF,
   MAIN_FSM_IDLE,
   MAIN_FSM_STREAM,
   MAIN_FSM_WAIT,
}MAIN_FSM;

typedef enum
{
   MATRIX_A,
   MATRIX_B,

}CURRENT_MATRIX;




#endif /* DEFINES_H_ */
